package com.sandeep.dataProviders;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataProviderExOne
{

    @Test(dataProvider = "Number")
    public void dataProviderExOne(int number1, int number2, int number3)
    {
        System.out.println("The number from the dataprovider is  " + number1);
        System.out.println("The number from the dataprovider is  " + number2);
        System.out.println("The number from the dataprovider is  " + number3);

    }

    @DataProvider(name = "Number")
    public Object[][] getdata()
    {

        Object[][] data = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

        return data;
    }

}
