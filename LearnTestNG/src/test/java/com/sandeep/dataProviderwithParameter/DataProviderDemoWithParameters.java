package com.sandeep.dataProviderwithParameter;

import org.testng.annotations.Test;

public class DataProviderDemoWithParameters
{

    @Test(dataProvider = "dp", dataProviderClass = DataProviderWithParameters.class)
    public static void dpdemo(int a, int b, int c)
    {
        int sum = 0;
        sum = a + b + c;
        System.out.println("The sum of the three integers are " + sum);
    }

    @Test(dataProvider = "dp", dataProviderClass = DataProviderWithParameters.class)
    public static void dpdemotwo(int a, int b, int c)
    {
        int sum = 0;
        sum = a + b + c;
        System.out.println("The sum of the three integers are " + sum);
    }

}
