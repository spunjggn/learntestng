package com.sandeep.dataProviderwithParameter;

import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;

public class DataProviderWithParameters
{

    @DataProvider(name = "dp")
    public Object[][] getdata(Method method)
    {
        if (method.getName().equalsIgnoreCase("dpdemo")) {
            Object[][] data = {{1, 2, 3}, {4, 5, 6}};
            return data;
        } else if (method.getName().equalsIgnoreCase("dpdemotwo")) {
            Object[][] data = {{11, 22, 33}, {44, 55, 66}};
            return data;
        }
        return null;
    }
}
