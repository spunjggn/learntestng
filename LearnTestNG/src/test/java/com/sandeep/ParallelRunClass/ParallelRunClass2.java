package com.sandeep.ParallelRunClass;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

public class ParallelRunClass2
{

    @Test
    public void TestMethod11(Method m)
    {

        System.out.println("The Method      " + m.getName() + "   of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());

    }

    @Test
    public void TestMethod12(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod13(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());

    }

    @Test
    public void TestMethod14(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());

    }

    @Test
    public void TestMethod15(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod16(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod17(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod18(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod19(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod20(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

}
