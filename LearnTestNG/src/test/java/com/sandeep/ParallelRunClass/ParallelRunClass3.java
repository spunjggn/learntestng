package com.sandeep.ParallelRunClass;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

public class ParallelRunClass3
{

    @Test
    public void TestMethod1(Method m)
    {

        System.out.println("The Method      " + m.getName() + "   of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());

    }

    @Test
    public void TestMethod2(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod3(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());

    }

    @Test
    public void TestMethod4(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());

    }

    @Test
    public void TestMethod5(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod6(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod7(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod8(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod9(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

    @Test
    public void TestMethod10(Method m)
    {

        System.out.println("The Method     " + m.getName() + "  of class " + this.getClass().getSimpleName()
            + " is getting executed by Thread  " + Thread.currentThread().getId());
    }

}
