package com.sandeep.TestPackage1;

import org.testng.annotations.Test;

public class ClassB
{

    @Test
    public static void Test1()
    {
        System.out.println("I am testmethod test1 of class ClassB  of package TestPackage1 ");
    }

    @Test
    public static void Test2()
    {
        System.out.println("I am testmethod test2 of class ClassB  of package TestPackage1 ");
    }

    @Test
    public static void Test3()
    {
        System.out.println("I am testmethod test3 of class ClassB  of package TestPackage1 ");
    }

}
