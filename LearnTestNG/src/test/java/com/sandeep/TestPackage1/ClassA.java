package com.sandeep.TestPackage1;

import org.testng.annotations.Test;

/**
 * The purpose of this project is to learn TestNG features 1st Priority to learn testng.xml
 * 
 * @version 1.0
 * @author Sandeep Punj
 */

public class ClassA
{
    /**
     * @category Sanity
     * @return void
     */
    @Test
    public static void Test1()
    {
        System.out.println("I am testmethod test1 of class ClassA  of package TestPackage1 ");
    }

    @Test
    public static void Test2()
    {
        System.out.println("I am testmethod test2 of class ClassA  of package TestPackage1 ");
    }

    @Test
    public static void Test3()
    {
        System.out.println("I am testmethod test3 of class ClassA  of package TestPackage1 ");
    }

}
