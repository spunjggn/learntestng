package com.sandeep.TestPackage1;

import org.testng.annotations.Test;

public class ClassC
{

    @Test
    public static void Test1()
    {
        System.out.println("I am testmethod test1 of class ClassC of package TestPackage1 ");
    }

    @Test
    public static void Test2()
    {
        System.out.println("I am testmethod test2 of class ClassC of package TestPackage1 ");
    }

    @Test(invocationCount = 3)
    public static void Test3()
    {
        System.out.println("I am testmethod test3 of class ClassC of package TestPackage1 ");
    }

}
