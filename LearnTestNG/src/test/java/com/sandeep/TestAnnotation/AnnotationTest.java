package com.sandeep.TestAnnotation;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author Sandeep Punj This class is an example to learn the use of Annotations of testng. This also describes
 *         the @Test(prioirty) to describer the priority of test cases.
 */

public class AnnotationTest
{

    @BeforeSuite
    public void beforeSuite()
    {

        System.out.println("I am the method with @Before Suite");
    }

    @BeforeMethod
    public void beforeMethod()
    {
        System.out.println("I am the method with @Before Method");

    }

    @BeforeClass
    public void beforeClass()
    {
        System.out.println("I am the method with @Before Class");
    }

    @BeforeTest
    public void beforeTest()
    {
        System.out.println("I am the method with @Before Test");
    }

    @Test(priority = 2, groups = "group2")

    public void test2()
    {
        System.out.println("I am the method with @Test2");
    }

    @Test(priority = 3, groups = "group2")
    public void test3()
    {
        System.out.println("I am the method with @Test3");
    }

    @Test(priority = 4, groups = "group2")
    public void test4()
    {
        System.out.println("I am the method with @Test4");
    }

    @Test(priority = 1, groups = "group1")
    public void test1()
    {
        System.out.println("I am the method with @Test1");
    }

    @AfterSuite
    public void afterSuite()
    {

        System.out.println("I am the method with @After Suite");
    }

    @AfterMethod
    public void afterMethod()
    {
        System.out.println("I am the method with @After Method");

    }

    @AfterClass
    public void afterClass()
    {
        System.out.println("I am the method with @After Class");
    }

    @AfterTest
    public void afterTest()
    {
        System.out.println("I am the method with @After Test");
    }

}
