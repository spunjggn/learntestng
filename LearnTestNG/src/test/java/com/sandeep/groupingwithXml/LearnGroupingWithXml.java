package com.sandeep.groupingwithXml;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

public class LearnGroupingWithXml
{

    @Test(groups = "One")
    public static void test1(Method method)

    {
        System.out.println("I am from Group One  " + method.getName());

    }

    @Test(groups = "One")
    public static void test2(Method method)

    {
        System.out.println("I am from Group One  " + method.getName());

    }

    @Test(groups = "Two")
    public static void test3(Method method)

    {
        System.out.println("I am from Group Two  " + method.getName());

    }

    @Test(groups = "Three")
    public static void test4(Method method)

    {
        System.out.println("I am from Group Three  " + method.getName());

    }

    @Test(groups = "One")
    public static void test5(Method method)

    {
        System.out.println("I am from Group One  " + method.getName());

    }

    @Test(groups = "Four")
    public static void test6(Method method)

    {
        System.out.println("I am from Group Four  " + method.getName());

    }

    @Test(groups = "Four")
    public static void test7(Method method)

    {
        System.out.println("I am from Group Four  " + method.getName());

    }

    @Test(groups = "Two")
    public static void test8(Method method)

    {
        System.out.println("I am from Group Two  " + method.getName());

    }

    @Test(groups = "Five")
    public static void test9(Method method)

    {
        System.out.println("I am from Group Five  " + method.getName());

    }

}
