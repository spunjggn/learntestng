package com.sandeep.TestPackage2;

import org.testng.annotations.Test;

public class ClassF
{
    @Test
    public static void Test1()
    {
        System.out.println("I am testmethod test1 of class ClassF  of package TestPackage2 ");
    }

    @Test
    public static void Test2()
    {
        System.out.println("I am testmethod test2 of class ClassF  of package TestPackage2 ");
    }

    @Test
    public static void Test3()
    {
        System.out.println("I am testmethod test3 of class ClassF  of package TestPackage2 ");
    }

}
