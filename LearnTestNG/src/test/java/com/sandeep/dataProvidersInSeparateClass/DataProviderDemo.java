package com.sandeep.dataProvidersInSeparateClass;

import org.testng.annotations.Test;

public class DataProviderDemo
{

    @Test(dataProvider = "dp", dataProviderClass = DataProviderInSeparateClass.class)
    public static void dpdemo(int a, int b, int c)
    {
        int sum = 0;
        sum = a + b + c;
        System.out.println("The sum of the three integers are " + sum);
    }

}
