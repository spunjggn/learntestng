package com.sandeep.dataProvidersInSeparateClass;

import org.testng.annotations.DataProvider;

public class DataProviderInSeparateClass
{

    @DataProvider(name = "dp")
    public Object[][] getdata()
    {
        Object[][] data = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        return data;

    }

}
