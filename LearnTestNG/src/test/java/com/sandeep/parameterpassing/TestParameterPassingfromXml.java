package com.sandeep.parameterpassing;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestParameterPassingfromXml
{
    @Parameters({"Name", "Standard"})
    @Test(priority = 1)
    public static void testParamter(String Name, String Standard)
    {
        System.out.println("The name of the student is " + Name + " and he is studies in Class" + Standard);
    }

    @Parameters("Game")
    @Test(priority = 2, dependsOnMethods = "testParamter")
    public static void test2Paramter(String Game)
    {
        System.out.println("The game loved by most of the indians is " + Game);
    }

}
