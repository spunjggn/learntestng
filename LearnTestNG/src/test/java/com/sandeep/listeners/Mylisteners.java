package com.sandeep.listeners;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class Mylisteners implements ITestListener
{

    @Override
    public void onTestStart(ITestResult result)
    {
        System.out.println(
            "My listner method invoked is onTestStart ,This is going to Print the method name which is about to Start "
                + result.getName());

    }

    @Override
    public void onTestSuccess(ITestResult result)
    {
        System.out
            .println("My listner method invoked is onTestSuccess ,This is going to Print the method name which Passes "
                + result.getName());

    }

    @Override
    public void onTestFailure(ITestResult result)
    {
        System.out
            .println("My listner method invoked is onTestFailure ,This is going to Print the method name which Fails "
                + result.getName());

    }

    @Override
    public void onTestSkipped(ITestResult result)
    {

        System.out.println(
            "My listner method invoked is onTestSkipped ,This is going to Print the method name which gets Skipped "
                + result.getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result)
    {

        System.out.println(
            "My listner method invoked is onTestFailedButWithinSuccessPercentage ,This is going to Print the method name which gets Passed with certain passes "
                + result.getName());
    }

    @Override
    public void onStart(ITestContext context)
    {
        System.out.println(
            "This is a Test Listeners which will <Test> tag name in xml and will be invoked earlier than method level listeners onStart "
                + context.getName());

    }

    @Override
    public void onFinish(ITestContext context)
    {
        System.out.println(
            "This is a Test Listeners which will <Test> tag name in xml and will be invoked latter than all method  listeners onFinish "
                + context.getName());

    }

}
