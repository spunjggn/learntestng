package com.sandeep.listeners;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.testng.IExecutionListener;

public class Iexecution implements IExecutionListener
{
    private long startTime;

    @Override
    public void onExecutionStart()
    {

        System.out.println("Test Run is about to Start");
        startTime = System.currentTimeMillis();
        String timestamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
        System.out.println("Test Run Started at " + timestamp);
        // System.out.println("It is run at the Suite level, so it runs @beforeSuite level");

    }

    @Override
    public void onExecutionFinish()
    {
        System.out.println("The Test Run ends at " + (System.currentTimeMillis() - startTime) + ":ms");
        System.out.println("The Test Run ends time is "
            + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime()));
    }
}
