package com.sandeep.listeners;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

public class IexecutionDemo
{

    @Test(invocationCount = 5)
    public void TestAB(Method m) throws InterruptedException
    {

        System.out.println(" Start of Method " + m.getName());
        Thread.sleep(5000);
    }

    @Test(invocationCount = 5)

    public void TestABC(Method m) throws InterruptedException
    {
        System.out.println(" Start of Method " + m.getName());
        Thread.sleep(5000);
    }

}
