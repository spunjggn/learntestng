package com.sandeep.listeners;

import java.lang.reflect.Method;

import org.junit.Assert;
import org.testng.annotations.Test;

public class ListenerDemo
{

    @Test
    public void TestA(Method method)
    {
        Assert.assertTrue(true);
        System.out.println("I am test Method TestA and will pass:: Method: " + method.getName());
    }

    @Test
    public void TestB(Method method)
    {
        Assert.assertTrue(false);
        System.out.println("I am test Method TestB and will fail:: Method: " + method.getName());
    }

    @Test(dependsOnMethods = "TestB")
    public void TestC(Method method)
    {
        System.out
            .println("I am test Method TestA and will skip as I am dependent on TestB :: Method: " + method.getName());
    }
}
