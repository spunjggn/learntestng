package com.sandeep.RetryAtRunTime;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Retry

{
    SoftAssert softassert = new SoftAssert();

    @Test
    public void retrialtest1()
    {
        System.out.println("I am retrialtest1 method");
        softassert.assertEquals(false, true);
        softassert.assertAll();
    }

    @Test
    public void retrialtest2()
    {
        Assert.assertEquals(true, true);
        System.out.println("I am retrialtest2 method");
        Assert.assertEquals(true, true);
    }

    @Test
    public void retrialtest3()
    {
        System.out.println("I am retrialtest3 method");
        softassert.assertEquals(false, true);
        softassert.assertAll();
    }

    @Test
    public void retrialtest4()
    {
        System.out.println("I am retrialtest4 method");
        Assert.assertEquals(true, true);

    }

    @Test
    public void retrialtest5()
    {
        System.out.println("I am retrialtest5 method");
        softassert.assertEquals(false, true);
        softassert.assertAll();
    }

}
